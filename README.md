Shared Computing Cluster (SCC) Cheat Sheet

Summary of common commands useful for working with the SCC. To build a PDF, run
the command:

```shell
pdflatex scc-cheatsheet.tex
```

Alternatively, you can use Pandoc to translate to other formats. For example, to
generate an HTML version:

```shell
module load texlive
module load pandoc
pandoc -f latex -t html scc-cheatsheet.tex -o scc-cheatsheet.html
```
